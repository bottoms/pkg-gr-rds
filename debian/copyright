Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gr-rds
Upstream-Contact: https://github.com/bastibl/gr-rds
Source:
 https://github.com/bastibl/gr-rds.git
 git archive --format=tar --prefix=gr-rds-1.1.0.f1c584a/ f1c584a  | xz > ../../gr-rds_3.8.0.0.f1c584a.orig.tar.xz
Copyright: 2013-2019 Bastian Bloessl <bloessl@ccs-labs.org>
 2013 Balint Seeber <balint@ettus.com>
 2010 Dimitrios Symeonidis
License: GPL-3+
Comment:
 brief: FM RDS/TMC Transceiver
 icon: https://fosdem.org/2015/schedule/event/sdr_rds_tmc/sdr_rds_tmc-c26370b6d55400089388d96b7d18d1b1067e997c76e8d8f1b2c6f94862866a1f.png

Files: COPYING
Copyright: 1989, 1991 Free Software Foundation, Inc.
License: COPYING
 Everyone is permitted to copy and distribute verbatim copies
 of this license document, but changing it is not allowed.

Files: .gitignore MANIFEST.md README.md cmake/* docs/*
 examples/* grc/* include/* lib/* python/*
Copyright: 2013-2019 Bastian Bloessl <bloessl@ccs-labs.org>
License: GPL-3+

Files: debian/*
Copyright: 2013-2015 A. Maitland Bottoms <bottoms@debian.org>
License: GPL-3+

Files: lib/tmc_locations_italy.h lib/constants.h lib/tmc_events.h
Copyright: 2004 Free Software Foundation, Inc.
License: GPL-2+

Files: CMakeLists.txt grc/CMakeLists.txt lib/CMakeLists.txt
 docs/CMakeLists.txt python/CMakeLists.txt python/__init__.py
 docs/doxygen/CMakeLists.txt docs/doxygen/doxyxml/base.py
 docs/doxygen/doxyxml/text.py docs/doxygen/doxyxml/doxyindex.py
 docs/doxygen/doxyxml/__init__.py
Copyright: 2003-2012 Free Software Foundation, Inc.
License: GPL-3+
Comment: inherited from GNU Radio

Files:  cmake/Modules/CMakeParseArgumentsCopy.cmake
Copyright: 2010 Alexander Neundorf <neundorf@kde.org>
License: Kitware-BSD

License: Kitware-BSD
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the names of Kitware, Inc., the Insight Software Consortium,
   nor the names of their contributors may be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-3'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".
